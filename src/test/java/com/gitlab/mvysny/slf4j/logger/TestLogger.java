package com.gitlab.mvysny.slf4j.logger;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Martin Vysny <mavi@vaadin.com>
 */
public class TestLogger implements HandroidLogger {
    public final List<String> messages = new ArrayList<>();

    @Override
    public void println(int priority, String name, String message, Throwable throwable) {
        messages.add(message);
    }

    public void assertMessages(String... messages) {
        assertArrayEquals(messages, this.messages.toArray());
    }

    @Override
    public String toString() {
        return "TestLogger{" +
                "messages=" + messages +
                '}';
    }
}
