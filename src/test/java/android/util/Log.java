package android.util;

import java.util.ArrayList;

/**
 * Mocked Android class for testing that logs everything to the list.
 */
public final class Log {
    /**
     * Output of the log filled by the [println] method.
     */
    public static ArrayList<String> output = new ArrayList<>();

    /**
     * Clears the [output]
     */
    public static void clear() {
        output.clear();
    }

    public static boolean isLoggable(java.lang.String tag, int level) {
        return true;
    }

    public static int println(int priority, java.lang.String tag, java.lang.String msg) {
        output.add(tag + ": " + msg);
        return 0;
    }
}
